#!/usr/bin/perl

use feature qw(:5.10);

use Time::gmtime;

if (defined($ARGV[0])) {
	if ($ARGV[0] =~ m/^\d+$/) {
		say(gmctime($ARGV[0]));
	} else {
		say STDERR ("usage: $0 [timestamp]");
		exit(1);
	}
} else {
	say(time());
}
