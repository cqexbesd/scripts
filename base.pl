#!/usr/bin/perl

# script to convert lists of numbers into other bases to simplify looking at
# binary data
#
# input options:
# 	-h	assume input is a series of numbers base16
#
# default is to assume decimal
#
# output options:
# 	-b	print values as binary
# 	-d	print values as decimal
# 	-x	print values as hex
# 	-a	print in all supported bases
#
# selecting any output option prodcues a "tabular" output. the default is to
# just print the numbers as a single hex string
#
# numbers can be space or comma space separated. lists can start and end with
# [ and ] respectively

use warnings;
use strict;
use feature qw(say);

use Getopt::Std;
use List::MoreUtils qw(natatime);

my @dec;
my %opts;

getopts('abdhx', \%opts);

# arguments might be a single string of numbers or multiple arguments of
# single numbers
# re to match hex or decimal numbers
my $num_re = $opts{h} ? qr/(?:[0-9a-fA-F])+/ : qr/\d+/;
while (my($i, $v) = each(@ARGV)) {
	my $re = qr/\s*($num_re)\s*,?/;
	if ($i == 0) {
		# first arg might have a [
		$re = qr/\[?/ . $re;
	}
	if ($i == $#ARGV) {
		# last arg might have a ]
		$re .= qr/\]?/;
	}

	my @new = ($v =~ /$re/g);
	@new = map { hex } @new if $opts{h};
	push(@dec, @new);
}

my $bytes_per_line = 8;
if ($opts{b} or $opts{d} or $opts{x} or $opts{a}) {
	# binary and/or decimal output
	my $it = natatime($bytes_per_line, @dec);
	my $n = 1;
	while (my @bytes = $it->()) {
		printf("Byte: % 5d" . "% 9d" x (scalar(@bytes) - 1) . "\n", map { $n++ } @bytes);
		printf("10" . " % 8u" x scalar(@bytes) . "\n", @bytes) if ($opts{d} or $opts{a});
		printf("0x" . "       %02x" x scalar(@bytes) . "\n", @bytes) if ($opts{x} or $opts{a});
		print("0b ", (map { unpack("B*", $_) . " " } (map { pack('C', $_) } @bytes)), "\n\n") if ($opts{b} or $opts{a});
	}
} else {
	# just hex
	printf("0x" . "%02x" x scalar(@dec) . "\n", @dec);
}
